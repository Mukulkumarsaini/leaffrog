const Sql = require('sequelize')
const models = require('../../models')
exports.AddProduct = async (req, res) => {
    try {
        let bodyData = req.body.data ? JSON.parse(req.body.data) : req.body;
        let newobject = {
            name: bodyData.name,
            discription: bodyData.discription,
            category: bodyData.category,
            subcategory: JSON.stringify(bodyData.subcategory),
            state: bodyData.state,
            gstrate: bodyData.gstrate,
            status: bodyData.state,
            price: Number(bodyData.price * bodyData.gstrate / 100),
            quality: bodyData.quality
        }
        let product_data = await models.Product.create(newobject)
        if (product_data) {
            let Manage = {
                product_id : product_data.id,
                product_name : product_data.name,
                quantity : bodyData.quantity
            }
            let manage_stock = await models.Managestock.create(Manage)
            if(!manage_stock){
                return res.json({ message: "something went wrong "}) 
            }
            return res.json({ message: "data saved", result: product_data })
        } else {
            return res.json({ message: "data  not saved" })
        }

    } catch (e) {
        return res.json({ message: "something went wrong", err: e })
    }
}
exports.getProduct = async (req, res) => {
    try {
        let product_data = await models.Product.findAll();
        if (product_data) {
            return res.json({ message: "data listed", result: product_data })
        } else {
            return res.json({ message: "data  not listed" })
        }
    } catch (e) {
        return res.json({ message: "something went wrong", err: e })
    }
}
exports.filterProduct = async (req, res) => {
    try {
        let searchValue = req.body.search;
        let searchData = {}
        if (searchValue) {
            searchData = {
                $or: [
                    {
                        state: {
                            $like: `%${searchValue}%`
                        }
                    },
                    {
                        category: {
                            $like: `%${searchValue}%`
                        }
                    },
                    {
                        subcategory: {
                            $like: `%${searchValue}%`
                        }
                    },

                ]
            }
            const product_data = await models.sequelize.query(`SELECT * FROM leaffrog.products where state like '%${searchValue}%' or category like '%${searchValue}%' or subcategory like '%${searchValue}%'`, {
                type: models.sequelize.QueryTypes.SELECT,
               // mapToModel: true // pass true here if you have any mapped fields
              });
            //   console.log(projects);
            // let product_data = await models.Product.qu({
            //     where:searchData
            // })

            if (product_data) {
                return res.json({ message: "data listed", result: product_data })
            } else {
                return res.json({ message: "data  not listed" })
            }

        }
    } catch (e) {
        console.log(e);
        return res.json({ message: "something went wrong", err: e })
    }
}
exports.Purchase = async(req,res)=>{
    try{
        let bodyData = req.body.data ? JSON.parse(req.body.data) : req.body;
        let stock_data = await models.Managestock.findAll({
            where : {
                product_id:bodyData.product_id
            }
        })
        if(stock_data[0].quantity < bodyData.quantity){
            res.json({message:"sorry this product is out of stock"})
        }
        let purchase_data = {
            product_id : bodyData.product_id,
            user_id : bodyData.user_id,
            quantity : bodyData.quantity
        }
        let sucess_purchase = await models.Parchase_product.create(purchase_data)
        if(sucess_purchase){
            let data = {
                quantity:stock_data[0].quantity - bodyData.quantity
            }
            let update_stock = await models.Managestock.update(data,{
                where :{
                    product_id:bodyData.product_id
                }
            })
            return res.json({message:"thanks for purchase" })
        }else{
            return res.json({ message: "something went wrong", err: e })
        }

        

    }catch(e){
        console.log(e);
        return res.json({ message: "something went wrong", err: e })
    }
}