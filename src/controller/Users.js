const Sql = require('sequelize')
const models = require('../../models')
const jwt = require('jsonwebtoken');
exports.Signup = async (req, res) => {
    try {
        let bodyData = req.body.data ? JSON.parse(req.body.data) : req.body;
        let newobject = {
            fname: bodyData.fname,
            lname: bodyData.lname,
            email: bodyData.email,
            password: bodyData.password,
            state: bodyData.state,
            mobile: bodyData.mobile
        }
        let user_data = await models.Users.create(newobject)
        if (user_data) {
            return res.json({ message: "data saved", result: user_data})
        } else {
            return res.json({ message: "data  not saved" })
        }

    } catch (e) {
        return res.json({ message: "something went wrong", err: e })
    }
}
exports.Login = async(req, res)=> {
    try {
        let bodyData = req.body.data ? JSON.parse(req.body.data) : req.body;
        console.log(bodyData);
        let login_data = await models.Users.findOne({
            where: {
                email: bodyData.email,
                password: bodyData.password
            }
        })
        if (login_data) {
            let token = await this.CreateToken(login_data)
            let sendData = {
                name: login_data.fname,
                email: login_data.email,
                phone: login_data.mobile,

            }
            if (token) {
                sendData.token = token
            }
            return res.json({ message: "successfully login", data: sendData })
        }
    } catch (e) {
        console.log(e);
        return res.json({ message: "something went wrong" })
    }

}
exports.CreateToken = async(data)=> {
    try {
       
        var token = '';
        let stoken = {
            id: data.id,
            email: data.email,
            name: data.fname,
        }
        token = jwt.sign(stoken, "interview", { expiresIn: '10d' });
        if (token) {
            return token
        }
        else return null
    } catch (e) {
        console.log("error in create token");
    }
}
exports.jwtDecode = async(req,res,next)=> {
    try {
        let token = req.headers.token
        console.log(token);
        let tokenData = await jwt.verify(token, "interview")
        console.log(tokenData);
        if (tokenData) {
           return next();
        }
    } catch (error) {
        console.log(error);
        return res.json({message:"failed authentication in jwt decode"})
    }

}
