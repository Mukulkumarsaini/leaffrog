var express = require('express');
var router = express.Router();
const Product = require('../src/controller/Product')
const Users = require('../src/controller/Users')
const jwt = require('jsonwebtoken');
/* GET users listing. */
router.post('/product',Product.AddProduct)
router.get('/get',Users.jwtDecode,Product.getProduct)
router.post('/filter',Users.jwtDecode,Product.filterProduct)
router.post('/sign',Users.Signup)
router.post('/login',Users.Login)
router.post('/purchase',Users.jwtDecode,Product.Purchase)
module.exports = router;
