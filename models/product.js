'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Product.init({
    name: DataTypes.STRING,
    discription: DataTypes.STRING,
    price: DataTypes.INTEGER,
    category: DataTypes.STRING,
    subcategory: DataTypes.STRING,
    state: DataTypes.STRING,
    quality: DataTypes.STRING,
    status: DataTypes.STRING,
    gstrate: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Product',
  });
  return Product;
};